
import express from 'express';
import 'module-alias/register';
import { initRoutes } from '@routes/route';

export class Server {
  /**
   * Variable application express
  */
  private readonly __app: express.Application = express();

  /**
   * Inital Server constructor
  */
  public constructor () {
      initRoutes(this.__app);
  }
  
  /**
   * App function
  */
  public get app(): express.Application {
      return this.__app;
  }
}