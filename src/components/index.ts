import { Router } from "express";
import { UserRoutes } from "@components/user/routes";

export function  registerRoutes(router: Router, prefix: string = '') {
  router.use(`${prefix}`, new UserRoutes().route)
}