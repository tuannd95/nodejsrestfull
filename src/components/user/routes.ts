import {Router} from 'express';
import { RoutesInterface } from '@routes/routeInterface';
import { UserController } from '@components/user/controller';
import { Validate } from './validate';
import { authMiddleware } from '@middleware/authMiddleware';

export class UserRoutes implements RoutesInterface<UserController> {

  /**
   * user controller avariable
  */
  public readonly controller: UserController;

  /**
   * route avariable
  */
  public readonly route: Router;

  /**
   * validate avariable
  */
  public readonly validate: Validate;

  /**
   * Inital User route constructor
  */
  public constructor() {
    this.controller = new UserController();
    this.route = Router();
    this.validate =  new Validate();
    this.initRoutes();
  }

  /**
   * Inital users route
   * 
   * @return void
  */
  initRoutes(): void {
    // users route
    this.route.get('/users', authMiddleware, this.controller.getUsers);
    this.route.post('/users/create', this.validate.postUser(), this.controller.postCreateUser);
    // auth route
    this.route.post('/auth/login', this.validate.login(), this.controller.login);
  }
}