import { bind } from 'decko';
import { NextFunction, Request, Response } from 'express';
import { validationResult } from 'express-validator';
import { User } from './model';
import { UserService } from './service';
import { OK, INTERNAL_SERVER_ERROR, UNPROCESSABLE_ENTITY, CREATED, BAD_REQUEST } from 'http-status-codes';
import { BaseController } from '@cores/controller/baseController';
import { hash } from '@services/helper/bcrypt';

export class UserController extends BaseController {

    /**
     * User service variable
    */
    private readonly userService: UserService;

    /**
     * Inital User Controller constructor 
     */
    public constructor() {
        super()
        this.userService = new UserService();
    }

    /**
     * Login
     * @param Request [body request]
     * @param Response [http response]
     * @param NextFunction [next function]
     * 
     * @return [return response json]
    */
    @bind
    public async login(req: Request, res: Response, next: NextFunction) : Promise<Response | void> {
      try {
        // Handler validate
        if (this.handlerValidateResponse(req, res)) return;
        let response = await this.userService.login(req.body.email, req.body.password);
        if (response.user_not_found) {
          this.errorResponse(res, BAD_REQUEST, 'Invalid email or password.');
          return;
        }
        delete response.user_not_found;
        this.successResponse(res, OK, response);
      } catch (error) {
        this.errorResponse(res, INTERNAL_SERVER_ERROR, 'Internal Server Error.', error);
      }
    }
    
    /**
     * Get list users
     * 
     * @param Request [body request]
     * @param Response [http response]
     * @param NextFunction [next function]
     * 
     * @return [return response json]
    */
    @bind
    public async getUsers(req: Request, res: Response, next: NextFunction) : Promise<Response | void> {
      const userAuth = this.getAuthContext(req);
      this.successResponse(res, OK, [userAuth]);
    }

    /**
     * Create new user method post
     * 
     * @param Request [body request]
     * @param Response [http response]
     * @param NextFunction [next function]
     * 
     * @return [return response json]
    */
    @bind
    public async postCreateUser(req: Request, res: Response, next: NextFunction) : Promise<Response | void> {
      try {
        // Handler validate
        if (this.handlerValidateResponse(req, res)) return;
        const {fullname, email, password} = req.body 
        const passwordHash = hash(password) 
        const user: User = await this.userService.createUser({fullname, email, password: passwordHash} as User)
        this.successResponse(res, CREATED, user)
      } catch (error) {
        this.errorResponse(res, INTERNAL_SERVER_ERROR, 'Internal Server Error.', error);
      }
    }
}