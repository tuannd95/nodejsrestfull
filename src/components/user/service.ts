import { User } from './model';
import { Repository, getManager } from 'typeorm';
import { compare } from '@services/helper/bcrypt';
import { JwtService } from '@services/jwt'
import { env } from '@config/environments';

export class UserService {

  /**
   * Initial repository
  */
  private repository: Repository<User>;

  /**
   * Initial jwt
  */
  private jwt: JwtService;

  /**
   * Inital user service contructor
  */
  public constructor() {
    this.repository = getManager().getRepository(User)
    this.jwt = new JwtService();
  }

  /**
   * Create new user
   * 
   * @param user [data user create]
   * 
   * @return [return user created]
  */ 
  public async createUser(request: User): Promise<User> {
    try {
      const user: User = await this.repository.save(request)
      return user
    } catch (error) {
      throw new Error(error);
    }
  }

  /**
   * Login
   * 
   * @param email [email]
   * @param password [password]
   * 
   * @return Object
  */
  public async login(email: String, password: String): Promise<{user_not_found: Boolean, access_token: String, refresh_token: String, expires_in: Number}> {
    try {
      let response = {
        user_not_found: false,
        access_token: '',
        refresh_token: '',
        expires_in: 0
      };
      const user = await this.repository.findOne({
        where: {
          email
        }
      })
      if (!user || !compare(password, user.password)) {
        response.user_not_found = true;
        return response;
      }
      const jwtPayload = {
        id: user.id,
        fullname: user.fullname,
        email: user.email,
      }
      const accessToken = await this.jwt.generateToken(jwtPayload, env.JWT.SECRET_KEY, Number(env.JWT.EXPIRES_IN));
      const refreshToken = await this.jwt.generateToken(jwtPayload, env.JWT.SECRET_KEY, Number(env.JWT.EXPIRES_IN) + 30);
      response.access_token = String(accessToken);
      response.refresh_token = String(refreshToken);
      response.expires_in = Number(env.JWT.EXPIRES_IN);
      return response; 
    } catch (error) {
      throw new Error(error)
    }
  }
}
