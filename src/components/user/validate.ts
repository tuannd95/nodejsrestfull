import { body } from 'express-validator'

export class Validate {
  /**
   * Validate create new user
  */
  public postUser():any {
    return [ 
      body('fullname', 'The fullname field is required.').exists(),
      body('email', 'The email field is invalid.').exists().isEmail(),
      body('password', "The password field is required.").exists()
    ]   
  }

  /**
   * Validate auth login
  */
  public login():any {
    return [
      body('email', 'The email field is required.').exists(),
      body('email', 'The email field is invalid.').isEmail(),
      body('password',"The password field is required.").exists()
    ];
  }
}
