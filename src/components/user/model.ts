import { Entity, Column } from "typeorm";
import { BaseModel } from "@cores/model/baseModel";

@Entity()
export class User extends BaseModel {

  @Column()
  public fullname!: string

  @Column({
    nullable: false,
    unique: true
  })

	public email!: string;

  @Column()
  public password!: string
}