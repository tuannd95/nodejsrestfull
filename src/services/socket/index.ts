import { socketInterface } from '@socket/index';
import { Socket } from 'socket.io';

const Sockets:any = {}

export class SocketService {
    
    /**
     * Register socket connection
     * 
     * @param identify [identify user connect socket]
     * @param connect [socket connection]
     * 
     * @return void
    */
    public registerClient(identify:any, connect:socketInterface) {
        Sockets[identify] = connect
    }

     /**
     * Remove socket connection
     * 
     * @param identify [identify user connect socket]
     * 
     * @return void
    */
    public removeClient(identify:any) {
        delete Sockets[identify];
    }

     /**
     * Send message socket for user
     * 
     * @param identify [identify user connect socket]
     * @param type [event type of socket]
     * @param message [message send to user]
     *
     * @return void
    */
    public sendMessageToUser(identify:any, type: String, message: String) {
        let connection = Sockets[identify];
        if (!connection) {
            throw {
                code: "NoSocketConnection"
            };
        }
        connection.emit(type, message);
    }

     /**
     * Send message excepted sender
     * 
     * @param identify [identify user connect socket]
     * @param type [event type of socket]
     * @param message [message send to user]
     *
     * @return void
    */
    public sendMessageExceptedSender(identify:any, type: String, message: String) {
        let connection = Sockets[identify];
        if (!connection) {
            throw {
                code: "NoSocketConnection"
            };
        }
        connection.broadcast.emit(type, message);
    }
}