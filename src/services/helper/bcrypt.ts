const bcrypt = require('bcrypt');

/**
 * Hash password
 * 
 * @param password [password]
 * @param saltRounds [salt Rounds, default 10]
 * 
 * @return string
*/
const hash = (password:String, saltRounds:Number = 10):String =>  bcrypt.hashSync(password, saltRounds);

/**
 * Compare password hash
 * 
 * @param password [password]
 * @param passwordHash [password hash]
 * 
 * @return boolean
*/
const compare = (password:String, passwordHash:any): Boolean => bcrypt.compareSync(password, passwordHash);

export {hash, compare} 