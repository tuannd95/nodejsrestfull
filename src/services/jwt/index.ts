const jwt = require('jsonwebtoken');

export class JwtService {
  /**
   * Default token expires is 1 hour
  */
  private defaultExpiresIn:Number = Math.floor(Date.now() / 1000) + (60 * 60);

  /**
   * JWT signin
   * 
   * @param data [data signin]
   * @param jwtSecretKey [jwt secret key]
   * @param expiresIn [token expires in, default is 1 hour]
   * @param alGorithm [algorithm, default is HS256]
   * 
   * @return
  */
  public generateToken(data: Object, jwtSecretKey:String, expiresIn:Number = this.defaultExpiresIn, alGorithm:String = 'HS256'): Object {
    return new Promise((resolve, reject) => {
      jwt.sign(data, jwtSecretKey, { algorithm: alGorithm, expiresIn }, (err:Error, token:String) => {
        if (err) {
          return reject(err);
        }
        return resolve(token);
      });
    }) 
  }

  /**
   * JWT verify token
   * 
   * @param token [jwt token]
   * @param jwtSecretKey [jwt secret key]
   * 
   * @return
  */
  public verifyToken(token:String, jwtSecretKey:String) {
    return new Promise((resolve, reject) => {
      jwt.verify(token, jwtSecretKey, (error: Error, decoded:any) => {
        if (error) {
          return reject(error);
        }
        resolve(decoded);
      });
    });
  }
}