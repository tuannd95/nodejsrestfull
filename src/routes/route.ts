import { Router } from 'express';
import { registerRoutes } from '@components/index';
import { appMiddleware } from '@middleware/appMiddleware';
import { handlerErrorMiddleware } from '@middleware/handlerErrorMiddleware';
import { env } from '@config/environments';

export function initRoutes(route: Router): void {
  const prefix: string = env.ROUTE_PREFIX_URL;
  appMiddleware(route);
  registerRoutes(route, prefix);
  handlerErrorMiddleware(route);
}