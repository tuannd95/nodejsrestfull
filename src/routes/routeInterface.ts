import { Router } from "express";

export interface RoutesInterface<T> {
  readonly controller: T;
  readonly route: Router;
  initRoutes() : void;
}