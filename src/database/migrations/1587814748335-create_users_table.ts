import {MigrationInterface, QueryRunner, Table, TableIndex} from 'typeorm';

export class createUsersTable1587814748335 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.createTable(new Table({
      name: 'users',
      columns: [
        {
          name: 'id',
          type: 'varchar',
          isPrimary: true
        },
        {
          name: 'fullname',
          type: 'varchar',
        },
        {
          name: 'email',
          type: 'varchar',
        },
        {
          name: 'password',
          type: "varchar",
        },
        {
          name: 'created_at',
          type: 'timestamp'
        },
        {
          name: 'updated_at',
          type: 'timestamp'
        }
      ]
    }), true)
    await queryRunner.createIndex("users", new TableIndex({
      name: "IDX_USERS_NAME",
      columnNames: ["email"]
    }));
  }

  public async down(queryRunner: QueryRunner): Promise<any> {
    await queryRunner.dropIndex("users", "IDX_USERS_NAME");
    await queryRunner.dropTable("users");
  }

}
