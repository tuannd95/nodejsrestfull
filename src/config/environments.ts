import { config } from 'dotenv';
config();

export const env = {
  'APP_PORT': process.env.APP_PORT || 3000,
  'APP_ENVIRONMENT': process.env.APP_ENVIRONMENT || 'development',
  'DIR_LOGGER': 'logs',
  'CORS_ORIGIN': ['*'],
  'JWT': {
    'SECRET_KEY': process.env.JWT_SECRET_KEY || '5E9F8A5981E62C77AEA6FA182BB4C',
    'EXPIRES_IN': process.env.JWT_EXPIRES_IN || 86400
  },
  'ROUTE_PREFIX_URL': '/api/v1'
}