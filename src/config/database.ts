import { ConnectionOptions } from 'typeorm';
import { config } from 'dotenv';
config();

const connectionOptions: ConnectionOptions = {
  type: 'postgres',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  synchronize: true,
  migrationsTableName: 'migrations',
  migrations: ['src/database/migrations/**/*.ts'],
  entities: [
    'src/components/**/model.ts'
  ],
  cli: {
    migrationsDir: 'src/database/migrations'
  }
}
 
export = connectionOptions;