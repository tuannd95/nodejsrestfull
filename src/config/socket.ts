export const SocketEventType = {
    authenticate: 'authenticate',
    unauthorized: 'unauthorized',
    authorized: 'authorized',
    disconnect: 'disconnect'
}