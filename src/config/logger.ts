import { existsSync, mkdirSync } from 'fs';
import { join } from 'path';
import { createLogger, format, transports } from 'winston';
import { env } from '@config/environments';
const DIR_LOG = env.DIR_LOGGER;

// Create the log directory if it does not exist
if (!existsSync(DIR_LOG)) {
	mkdirSync(DIR_LOG);
}

const errorLog = join(DIR_LOG, 'error.log');
const requestLog = join(DIR_LOG, 'request.log');
const combinedLog = join(DIR_LOG, 'combined.log');
const exceptionsLog = join(DIR_LOG, 'exceptions.log');

const isRequest = format((info, opts) => {
	if (info.isRequest) {
		return info;
	}
	return false;
});

export const logger = createLogger({
	level: 'info',
	format: format.combine(
		format.timestamp({
			format: 'YYYY-MM-DD HH:mm:ss'
		}),
		format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
	),
	transports: [
		new transports.File({
			filename: errorLog,
			level: 'error'
		}),
		new transports.File({
			filename: requestLog,
			format: format.combine(isRequest())
		}),
		new transports.File({
      filename: combinedLog
		})
	],
	exceptionHandlers: [
		new transports.File({
			filename: exceptionsLog
		})
	]
});

if (env.APP_ENVIRONMENT !== 'production') {
	logger.add(
		new transports.Console({
			format: format.combine(
				format.colorize(),
				format.printf((info) => `${info.timestamp} ${info.level}: ${info.message}`)
			),
			level: 'debug'
		})
	);
}