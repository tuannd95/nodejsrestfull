import { Router, Request, Response, NextFunction } from 'express';

export function handlerErrorMiddleware(router: Router): Response | void {
  
  router.use((req: Request, res: Response, next: NextFunction) => {
    return res.status(404).json({
      error: "Page Not Found",
      status: 404
    })
  });
  
	router.use((err: Error, req: Request, res: Response, next: NextFunction) => {
		return res.status(500).json({
			error: err.message || err,
			status: 500
		});
	});
}