import { Request, Response, NextFunction } from 'express';
import { UNAUTHORIZED } from 'http-status-codes'
import { JwtService } from '@services/jwt'
import { env } from '@config/environments';
import {getRepository } from 'typeorm';
import { User } from '@components/user/model';

interface baseResponseInterface {
  status:String,
  code:Number,
  error:String
}

export function authMiddleware(req: Request, res: Response, next: NextFunction): Response | void {
  let authorization = String(req.headers.authorization);
  let authorizationSplit = authorization.split(' ');
  let authorizationResponse: baseResponseInterface = {
    status: 'error',
    code: UNAUTHORIZED,
    error: '401 Unauthorized'
  }
  if (authorizationSplit[0] !== 'Bearer') {
    res.status(UNAUTHORIZED).json(authorizationResponse)
    return;
  }
  const jwt = new JwtService();
  jwt.verifyToken(authorizationSplit[1], env.JWT.SECRET_KEY).then((data:any) => {
    getRepository(User).findOne(data.id).then((item:any) => {
      if (item) {
        req.app.set('user', data)
        return next();
      } else {
        res.status(UNAUTHORIZED).json(authorizationResponse);
        return;
      }
    }).catch(err => {
      res.status(UNAUTHORIZED).json(authorizationResponse)
      return;
    });
  }).catch(err => {
    res.status(UNAUTHORIZED).json(authorizationResponse);
    return;
  });
}