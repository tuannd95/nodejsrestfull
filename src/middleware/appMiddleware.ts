import { Router, Request, Response, NextFunction, json } from 'express';
import { logger } from '@config/logger';
import helmet from 'helmet';
import cors from 'cors';
import { env } from '@config/environments';

export function appMiddleware(router: Router): void {
  router.use(helmet());
  if (env.APP_ENVIRONMENT === 'production') {
    router.use(
      cors({origin: env.CORS_ORIGIN})
    )
  } else {
    router.use(
      cors({origin: '*'})
    )
  }
  router.use(json());

  router.use((req : Request, res: Response, next: NextFunction) => {
    const ip: string | string[] | undefined = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    logger.log({
      isRequest: true,
      level: 'info',
      message: `${req.method} ${req.url} ${ip}`
    });
    return next();
  })
}