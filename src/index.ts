import * as express from "express";
import 'module-alias/register';
import 'reflect-metadata';
import {Server} from "./server";
import { createServer, Server as HttpServer } from 'http';
import { logger } from '@config/logger';
import { env } from '@config/environments';
import {createConnection} from "typeorm";
import connectionOptions from '@config/database';
import { initSocket } from './socket';

(async function main() {
  try {
    // Connecting database
    const connection = await createConnection(connectionOptions);
    logger.info('Connection database has been successfully.')
    // Initial app
    const app: express.Application = new Server().app;
    const server: HttpServer = createServer(app);
    // Initial socket server
    initSocket(server);
    server.listen(env.APP_PORT)
    server.on('listening', () => {
      logger.info(`REST API listening at http://localhost:${env.APP_PORT}`);
    })
    server.on('close', () => {
      connection.close();
      logger.info('REST API server closed');
    });
  } catch (error) {
    logger.error(`error: ${error.stack}`);
  }
})();