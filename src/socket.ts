import { Server as HttpServer } from 'http';
import io from 'socket.io';
import { SocketEvent } from './socket/index';
import { SocketEventType } from '@config/socket';
import { socketInterface } from '@socket/index';

export function initSocket(server: HttpServer) {

    const socket = io(server,{path: 'socket'})
    socket.on("connection", socket => {
        Object.keys(SocketEvent).forEach(event => {
            socket.on(event, data => {
                if (event !== SocketEventType.authenticate) {
                    socket.emit("Unauthorized");
                    return;
                }
                SocketEvent[event](data, socket as socketInterface);
            });
        });
    });
}