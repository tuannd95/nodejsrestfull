import io from 'socket.io';
import { JwtService } from '@services/jwt'
import { UNAUTHORIZED } from 'http-status-codes'
import { SocketEventType } from '@config/socket';
import { env } from '@config/environments';
import {getRepository } from 'typeorm';
import { User } from '@components/user/model';
import { SocketService } from '@services/socket/index';
import { socketInterface } from '@socket/index';

export function onAuthenticate (data:any, socket: socketInterface) {
  
  try {
    if (!data.token) {
      SocketUnauthorized(socket);
      return;
    }
    const tokens = data.token.split(/ /);
    if (tokens.length !== 2 || tokens[0] !== "Bearer") {
      SocketUnauthorized(socket);
      return;
    }
    // handler check token
    const jwt = new JwtService();
    jwt.verifyToken(tokens[0], env.JWT.SECRET_KEY).then((data:any) => {
      getRepository(User).findOne(data.id).then((item:any) => {
        if (item) {
          socket.user = item;
          const socketService = new SocketService();
          socketService.registerClient(item.id, socket)
          socket.emit(SocketEventType.authorized);
          return;
        } else {
          SocketUnauthorized(socket);
          return;
        }
      }).catch(err => {
        SocketUnauthorized(socket);
        return;
      });
    }).catch(err => {
      SocketUnauthorized(socket);
      return;
    });
  } catch (error) {
    SocketUnauthorized(socket);
    return;
  } 
}

function SocketUnauthorized(socket: io.Socket) {
  const authErrorResponse = {
    status: 'error',
    code: UNAUTHORIZED,
    error: '401 Unauthorized'
  }  
  socket.emit(SocketEventType.unauthorized, authErrorResponse)
  socket.disconnect();
  return;
}