import { SocketEventType } from '@config/socket';
import { onAuthenticate } from './onAuthenticate';
import { onDisconnect } from './onDisconnect';
import io from 'socket.io';
import { User } from '@components/user/model';

export interface socketInterface extends io.Socket {
    user: User
}
 
export const SocketEvent = {
    [SocketEventType.authenticate] : onAuthenticate,
    [SocketEventType.disconnect]: onDisconnect
}