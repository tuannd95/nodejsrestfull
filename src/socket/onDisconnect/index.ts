import { socketInterface } from '@socket/index';
import { SocketService } from '@services/socket';

export function onDisconnect (data:any, socket:socketInterface) {
    const socketService = new SocketService();
    socketService.removeClient(socket.user.id);
}