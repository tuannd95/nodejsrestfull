import { Column, Timestamp, PrimaryGeneratedColumn } from 'typeorm';

export abstract class BaseModel {
  @PrimaryGeneratedColumn('uuid')
  public id!: string;
  
  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  public created_at!: Timestamp

  @Column({ type: "timestamp", default: () => "CURRENT_TIMESTAMP"})
  public updated_at!: Timestamp
}