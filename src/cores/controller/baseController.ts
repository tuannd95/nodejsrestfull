import { Request, Response } from 'express';
import { OK, CREATED, INTERNAL_SERVER_ERROR, UNPROCESSABLE_ENTITY} from 'http-status-codes'
import { logger } from '@config/logger';
import { validationResult } from 'express-validator';

interface baseResponseInterface {
  status:String,
  code:Number,
  data: any,
  validate_errors?:Array<any>,
  error?:String
}

interface authContextInterface {
  id: String,
  email: String,
  fullname: String
}

export abstract class BaseController {

  /**
   * Success response function
   * 
   * @param res [response]
   * @param statusCode [status code]
   * @param data [data response]
   *
   * @return objects
  */
  public successResponse(res:Response, statusCode: number, data:any = null): Object {
    return res.status(statusCode).json(this.baseResponse(statusCode, data));
  }

  /**
   * Error response function
   * 
   * @param res [response]
   * @param statusCode [status code]
   * @param errors [errors response]
   *
   * @return objects
  */
  public errorResponse(res:Response, statusCode: number, error: string, errors:Array<any> = []): Object {
    if (statusCode === INTERNAL_SERVER_ERROR) {
      logger.error('throw error:', errors)
      errors = [];
      error = 'Internal Server Error.'
    }
    return res.status(statusCode).json(this.baseResponse(statusCode, [], error, errors));
  }

  /**
   * Base response function
   * 
   * @param code [status code]
   * @param data [data response]
   * @param errors [errors response]
   *
   * @return objects
  */
  private baseResponse(code: number, data: any = [], error: String = '', validate_errors: Array<any> = []): Object {
    let baseResponse: baseResponseInterface = {
      status: code === OK || code === CREATED ? 'success' : 'error',
      code,
      data
    }
    if (code != OK && code != CREATED) {
      baseResponse.validate_errors = validate_errors;
      baseResponse.error = error
    }
    return baseResponse
  }

  /**
   * Handler response validate request
   * 
   * @param req [Request]
   * @param res [Response]
   *
   * @return Boolean
  */
  public handlerValidateResponse(req: Request, res: Response): Boolean {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      this.errorResponse(res, UNPROCESSABLE_ENTITY, '422 Unprocessable Entity', errors.array());
      return true
    }
    return false
  }

  /**
   * Get user authentication
   * 
   * @param req [Request]
   *
   * @return authContextInterface
  */
  public getAuthContext(req: Request): authContextInterface {
    const authContext:authContextInterface = req.app.get('user');
    return authContext
  }
}