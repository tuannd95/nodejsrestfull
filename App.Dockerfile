FROM node:12-alpine

RUN mkdir -p /home/node/app

WORKDIR /home/node/app

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

COPY package.json ./

RUN yarn

COPY . .

EXPOSE 3000

CMD [ "yarn", "start:watch" ]